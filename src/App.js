import React, { Component } from "react";
import Dashboard from "./components/Dashboard";
import Header from "./components/layout/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import AddUser from "./components/user/AddUser";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/addUser" component={AddUser} />
        </div>
      </Router>
    );
  }
}
export default App;
