import React, { Component } from "react";

class AddUser extends Component {
  constructor() {
    super();
    this.state = {
      firstName: "",
      lastName: "",
      gender: "",
      email: "",
      password: "",
      tel: "",
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onSubmit(e) {
    e.preventDefault();
    const newUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      gender: this.state.gender,
      email: this.state.email,
      password: this.state.password,
      tel: this.state.tel,
    };
    console.log(newUser);
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8 m-auto">
            <h1 className="display-4 text-center">Inscription</h1>
            <p className="lead text-center">Créer votre compte</p>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="Votre prénom"
                  name="firstName"
                  value={this.state.firstName}
                  onChange={this.onChange}
                  required
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="Votre nom"
                  name="lastName"
                  value={this.state.lastName}
                  onChange={this.onChange}
                  required
                />
              </div>
              <div className="form-group">
                <select
                  name="gender"
                  className="form-control form-control-lg"
                  value={this.state.gender}
                  onChange={this.onChange}
                >
                  <option value="Feminin">Feminin</option>
                  <option value="Masculin">Masculin</option>
                </select>
              </div>
              <div className="form-group">
                <input
                  type="email"
                  className="form-control form-control-lg"
                  placeholder="Adresse mail"
                  name="email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <input
                  type="password"
                  className="form-control form-control-lg"
                  placeholder="Mot de passe"
                  name="password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="Votre telephone"
                  name="tel"
                  value={this.state.tel}
                  onChange={this.onChange}
                />
              </div>
              <input type="submit" className="btn btn-info btn-block mt-4" />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AddUser;
